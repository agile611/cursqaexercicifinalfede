package com.itnove.crm.search;

import com.itnove.ba.BaseTest;
import com.itnove.crm.pages.DashboardPage;
import com.itnove.crm.pages.LoginPage;
import com.itnove.crm.pages.SearchResultsPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchNoResultsTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        String keyword = "castanya";
        //Accedir a pagina
        driver.navigate().to("http://crm.votarem.lu");
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.search(driver,wait,hover,keyword);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);
        assertTrue(searchResultsPage.isNoSearchResultsDisplayed(wait));
    }
}
